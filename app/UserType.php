<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Speciality;

class UserType extends Model
{
    protected $guarded = ['id'];

    const CLIENT = 1;
    const VET = 2;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pets()
    {
        return $this->hasMany(Pet::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship
     *
     * @return void
     */
    public function specialities(){
        return $this->hasMany(Speciality::class);
    }
}
