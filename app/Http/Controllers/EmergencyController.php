<?php

namespace App\Http\Controllers;

use App\UserType;
use App\Emergency;
use App\Pet;
use Illuminate\Http\Request;

class EmergencyController extends Controller
{
    public function index()
    {
        $userType = UserType::where('user_id', auth()->id())->first();
        $pets = $userType->pets()->get();
        $emergencies = Pet::select('emergencies.*')->join('emergencies', 'emergencies.pet_id', '=', 'pets.id')->where('user_type_id', $userType->id)->get();

        return view('emergency.index', compact(['emergencies', 'pets']));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $client = UserType::where('user_id', auth()->id())->first();
        $pets = $client->pets()->get();

        return view('emergency.register', compact(['pets']));
    }

    /**
     * @param Emergency $emergency
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Emergency $emergency)
    {
        $emergency->add_information = \request()->get('add_information');
        $emergency->save();

        return redirect()->action('EmergencyController@index')->with('success', 'Emergência editada com sucesso!');
    }

    /**
     * @param Emergency $emergency
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Emergency $emergency)
    {

        return view('emergency.edit', compact('emergency'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        Emergency::create([
            'pet_id' => $data['petId'],
            'about' => $data['about']
        ]);

        return redirect()->action('EmergencyController@index')->with('success', 'Emergência criada com sucesso!');
    }
}
return redirect()->action('PetController@index');
