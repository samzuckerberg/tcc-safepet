<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;
use App\UserType;

class UserController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        return view('user.index', compact(['user']));
    }

    public function update(User $editUser)
    {
        $data = request()->all();

        $editUser->name = $data['name'];
        $editUser->email = $data['email'];
        $editUser->password = Hash::make($data['password']);
        $editUser->save();

        $userType = UserType::where('user_id', $editUser->id)->first();

        $userType->contact = $data['contact'];
        $userType->cpf = $data['cpf'];
        $userType->birth = $data['birth'];
        $userType->address = $data['address'];
        $userType->state_id = $data['state'];
        $userType->city = $data['city'];
        $userType->save();

        return redirect()->action('HomeController@index')->with(' success ', 'Usuário editado com sucesso!');
    }
}
