<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clinic;
use App\Speciality;
use App\UserType;
use App\Scheduling;
use Illuminate\Support\Facades\Validator;
use App\Emergency;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clinics = Clinic::all();
        $client = UserType::where('user_id', auth()->id())->first();
        $pets = $client->pets()->get();

        if (\Session::get('user_type') == 1) {
            return view('appointment.index', compact('clinics', 'pets'));
        }
        $schedulings = Scheduling::where('user_type_id', auth()->user()->userType()->first()->id)->get();
        $emergencies = Emergency::all();
        return view('appointment.appointments', compact('schedulings', 'emergencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->validator($data);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        Scheduling::create([
            'user_type_id' => $data['user_type_id'],
            'dateTime' => $data['dateTime'],
            'pet_id' => $data['pet_id'],
            'adds' => $data['adds'],
            'payment_method' => $data['payment_method'],
            'status' => Scheduling::STATUS_WAITING
        ]);

        return redirect()->action('AppointmentController@index')->with('success', 'Consulta registrada com sucesso!');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'payment_method' => ['required'],
            'adds' => ['required', 'string'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Clinic $appointment)
    {
        $data = Speciality::where('clinic_id', $appointment->id)->with('userType')->with('userType.user')->get();

        return view('appointment.vets', compact('data', 'appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return view confirm
     *
     * @return void
     */
    public function confirm(Request $request)
    {
        $data = $request->all();
        $data['vet_data'] = json_decode($data['vet_data']);
        $client = UserType::where('user_id', auth()->id())->first();
        $pets = $client->pets()->get();
        $date = $request->get('date');

        return view('appointment.confirm', compact('data', 'pets', 'date'));
    }

    /**
     * Undocumented function
     *
     * @param Scheduling $scheduling
     * @return void
     */
    public function changeStatus(Scheduling $scheduling)
    {
        $scheduling->status = 1;
        $scheduling->save();
        return redirect()->action('AppointmentController@index')->with('success', 'Consulta agendada com sucesso!');
    }

    /**
     * Undocumented function
     *
     * @param Emergency $emergency
     * @return void
     */
    public function confirmEmergency(Emergency $emergency)
    {
        $emergency->status = 'a';
        $emergency->save();
        return redirect()->action('AppointmentController@index')->with('success', 'Consulta agendada com sucesso!');
    }
}
