<?php

namespace App\Http\Controllers;

use App\UserType;
use App\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PetController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $client = UserType::where('user_id', auth()->id())->first();
        $pets = $client->pets()->get();

        return view('pet.index', compact(['pets']));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('pet.create');
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'min:1', 'max:100'],
            'birth' => ['required', 'date'],
            'species' => ['required', 'min:1', 'max:100'],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->validator($data);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $userType = UserType::where('user_id', auth()->id())->first();


        try {
            Pet::create([
                'user_type_id' => $userType->id,
                'name' => $data['name'],
                'birth' => $data['birth'],
                'species' => $data['species'],
            ]);
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Data inválida');
        }

        return redirect()->action('PetController@index')->with('success', 'Pet registrado com sucesso!');
    }

    /**
     * @param Pet $pet
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Pet $pet)
    {
        return view('pet.edit', compact(['pet']));
    }

    /**
     * @param Pet $pet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Pet $pet)
    {
        try {
            $pet->name = \request()->get('name');
            $pet->birth = \request()->get('birth');
            $pet->species = \request()->get('species');

            $pet->save();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Data inválida');
        }

        return redirect()->action('PetController@index')->with('success', 'Aumiguinho editado com sucesso!');
    }

    public function destroy(Pet $pet)
    {
        if ($pet->emergency()->get()->isNotEmpty())
            return redirect()->back()->withErrors('O seu AUmiguinho tem consulta registrada, portanto não pode ser removido ;)');


        $pet->delete();

        return redirect()->action('PetController@index')->with('success', 'AUmiguinho removido com sucesso!');
    }
}
