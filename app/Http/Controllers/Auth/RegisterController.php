<?php

namespace App\Http\Controllers\Auth;

use App\City;
use App\UserType;
use App\Role\UserRole;
use App\State;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use PragmaRX\Countries\Package\Countries;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'contact' => ['required', 'min:1', 'max:15'],
            'cpf' => ['required', 'min:1', 'max:20'],
            'birth' => ['required'],
            'state' => ['required', 'max:20'],
            'address' => ['required', 'max:50'],
            'city' => ['required', 'max:50'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        try {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                // 'roles' => UserRole::ROLE_CLIENT
            ]);
            UserType::create([
                'user_id' => $user->id,
                'contact' => $data['contact'],
                'cpf' => $data['cpf'],
                'birth' => $data['birth'],
                'state_id' => $data['state'],
                'address' => $data['address'],
                'city' => $data['city'],
                'type' => $data['type'],
                'crmv' => isset($data['crmv']) ? $data['crmv'] : null
            ]);
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Informações inválidas!');
        }
        \Session::put('user_type', $data['type']);
        return $user;
    }
}
