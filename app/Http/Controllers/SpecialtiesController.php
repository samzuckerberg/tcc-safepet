<?php

namespace App\Http\Controllers;

use App\Specialties;
use Illuminate\Http\Request;
use App\Clinic;
use Illuminate\Support\Facades\Validator;
use App\Speciality;

class SpecialtiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialties = Speciality::select('specialities.id', 'clinics.name', 'specialities.average_value')
            ->join('clinics', 'clinics.id', '=', 'clinic_id')->where('user_type_id', auth()->user()->userType->id)->get();

        return view('speciality.index', compact('specialties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clinics = Clinic::all();

        return view('speciality.create', compact('clinics'));
    }

    /**
     * Validator
     *
     * @param array $data
     * @return void
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'clinics_id' => ['required', 'integer'],
            'average_value' => ['required', 'string'],
            'university_gradutate' => ['required', 'string'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->validator($data);
        if ($validator->fails())
            return redirect()->back()->withErrors($validator->errors());

        Speciality::create(
            [
                'user_type_id' => auth()->user()->userType->id,
                'clinic_id' => $data['clinics_id'],
                'average_value' => $data['average_value'],
                'university_graduate' => $data['university_gradutate']
            ]
        );

        return redirect()->action('SpecialtiesController@index')->with('success', 'Especialidade registrada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Specialties  $specialties
     * @return \Illuminate\Http\Response
     */
    public function show(Speciality $specialty)
    {
        $specialties = Speciality::select('specialities.*', 'clinics.name')
            ->join('clinics', 'clinics.id', '=', 'clinic_id')->where('specialities.id', $specialty->id)->first();
        $clinics = Clinic::all();
        return view('speciality.edit', compact('specialties', 'clinics'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Specialties  $specialties
     * @return \Illuminate\Http\Response
     */
    public function edit(Speciality $specialty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Specialties  $specialties
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Speciality $specialty)
    {
        $data = $request->all();

        $validator = $this->validator($data);
        if ($validator->fails())
            return redirect()->back()->withErrors($validator->errors());

        $specialty->clinic_id = $data['clinics_id'];
        $specialty->average_value = $data['average_value'];
        $specialty->university_graduate = $data['university_gradutate'];
        $specialty->save();
        return redirect()->action('SpecialtiesController@index')->with('success', 'Especialidade editada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Specialties  $specialties
     * @return \Illuminate\Http\Response
     */
    public function destroy(Speciality $specialty)
    {
        $specialty->delete();
        return redirect()->action('SpecialtiesController@index')->with('success', 'Especialidade removida com sucesso!');
    }
}
