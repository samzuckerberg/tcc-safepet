<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduling extends Model
{
    protected $guarded = ['id'];

    const STATUS_WAITING = 0;
    const STATUS_CONFIRMED = 1;
}
