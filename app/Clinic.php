<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    /**
     * Relationship
     *
     * @return void
     */
    public function specialities(){
        return $this->hasMany(Speciality::class);
    }
}
