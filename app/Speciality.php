<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $guarded = ['id'];

    /**
     * Relationship
     *
     * @return void
     */
    public function userType()
    {
        return $this->belongsTo(UserType::class);
    }

    /**
     * Relationship
     *
     * @return void
     */
    public function clinics()
    {
        return $this->belongsTo(Clinic::class);
    }
}
