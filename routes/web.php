<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

Route::resource('/emergency', 'EmergencyController');
Route::resource('/pets', 'PetController');
Route::resource('/appointment', 'AppointmentController');
Route::post('/appointment/confirm', 'AppointmentController@confirm');
Route::post('/appointment/changeStatus/{scheduling}', 'AppointmentController@changeStatus');
Route::post('/appointment/confirmEmergency/{emergency}', 'AppointmentController@confirmEmergency');
Route::resource('/specialties', 'SpecialtiesController');
Route::resource('/edit-user', 'UserController');
