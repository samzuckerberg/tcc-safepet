@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="background-color: #00C1AF">
                    <div class="d-flex justify-content-between">
                            <button type="button" style="max-width: 300px"
                    class="btn btn-light btn-lg text-truncate w-100">{{$appointment->name}}</button>

                            <div class="input-group mb-3" style="max-width: 250px">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Ordenar por</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option value="1">{{__('Menor valor')}}</option>
                                    <option value="2">{{__('Maior valor')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (\Session::get('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{\Session::get('success')}}</li>
                </ul>
            </div>
            @endif
            @foreach($data as $speciality)
            <div class="mt-4 col-12">
                <div>
                    <div class="card">
                    <div class="card-body">
                        <form method="post" action="/appointment/confirm">
                            @csrf
                            <div class="d-flex justify-content-between">
                                <div class="">
                                    <img src="/img/appointment-dog01.jpg" class="rounded-circle" style="max-width: 100px">
                                    <div class="">
                                    <h6 class="font-weight-bold mt-3" style="color: #00C1AF">R$ {{$speciality->average_value}}</h6>
                                    </div>

                                </div>
                                <div>
                                    <h4>{{$speciality->userType->user->name}}</h4>

                                <h4>{{__('Graduação')}}: {{$speciality->university_graduate}}</h4>
                                    <input class="form-control" required name="date" type="datetime-local">
                                </div>
                            <input type="hidden" name="vet_data" value="{{$speciality}}">
                                <div>
                                    <div>
                                    <h5>CRMV: {{$speciality->userType->crmv}}</h5>
                                    <button type="submit"
                                    class="btn btn-secondary text-truncate mt-3 float-right">{{__('Agendar')}}</button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </for>
                </div>
            </div>
            @endforeach
            @if(!isset($data[0]))
             <div class="alert alert-warning w-100">
                <ul>
                    <li>{{__('Não existem veterinários com essa especificação')}}</li>
                </ul>
            </div>

            @endif
        </div>
    </div>
    @endsection
