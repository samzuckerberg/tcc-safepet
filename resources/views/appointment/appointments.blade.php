@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body" style="background-color: #45B39D;">
                    <div class="d-flex">
                        <div class="col d-flex flex-column justify-content-center">
                            <h4 class="text-light line">{{__('Veja aqui as suas consultas!')}}</h4>
                            <div class="">
                                <h5 class="pt-5">
                                    {{__('Veja os confirmados e as consultas que estão esperando a sua confirmação!')}}
                                </h5>
                            </div>
                        </div>
                        <img class="col-6" src="img/appointment-dog01.jpg" style="max-height: 250px">
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (\Session::get('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{\Session::get('success')}}</li>
                </ul>
            </div>
            @endif
            <div class="container mt-4">
                <div class="row">
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Pet</th>
                        <th scope="col">{{__('Data e horário')}}</th>
                        <th scope="col">{{__('Método de pagamento')}}</th>
                        <th scope="col">{{__('Status')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($schedulings as $dataIndex => $data)
                        <tr>
                        <th scope="row">{{$dataIndex+1}}</th>
                        <td>{{App\Pet::where('id', $data->pet_id)->first()->name}}</td>
                        <td>{{$data->dateTime}}</td>
                        <td>@if($data->payment_method == 1)Dinheiro @endif
                        @if($data->payment_method == 2)Débito @endif
                    @if($data->payment_method == 3)Crédito @endif</td>
                        <td>@if($data->status)
                                <button type="button" disabled class="btn btn-primary">Agendado</button>
                            @else
                        <form method="post" action="/appointment/changeStatus/{{$data->id}}">
                            @csrf
                                <button type="submit" class="btn btn-primary">Agendar</button>
                            </form>
                            @endif
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                <div class="row">
                    <h2>Emergência</h2>
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Pet</th>
                        <th scope="col">{{__('Sobre')}}</th>
                        <th scope="col">{{__('Informação Adicional')}}</th>
                        <th scope="col">{{__('Status')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($emergencies as $emergencyIndex => $emergency)
                        <tr>
                        <th scope="row">{{$emergencyIndex+1}}</th>
                        <td>{{App\Pet::where('id', $emergency->pet_id)->first()->name}}</td>
                        <td>{{$emergency->about}}</td>
                        <td>{{$emergency->add_information}}</td>
                        <td>@if($emergency->status === 'a')
                                <button type="button" disabled class="btn btn-primary">Agendado</button>
                            @else
                        <form method="post" action="/appointment/confirmEmergency/{{$emergency->id}}">
                            @csrf
                                <button type="submit" class="btn btn-primary">Agendar</button>
                            </form>
                            @endif
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
