@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="background-color: #00C1AF">
                    <div class="d-flex justify-content-between">
                        {{-- @dd($data['vet_data']->user_type) --}}
                        <div>
                            <h4>{{$data['vet_data']->user_type->user->name}}</h4>
                        </div>
                        <div>
                            <h4>CRMV: {{$data['vet_data']->user_type->crmv}}</h4>
                        </div>
                    </div>
                <h5>Graduação: {{$data['vet_data']->university_graduate}}</h5>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (\Session::get('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{\Session::get('success')}}</li>
                </ul>
            </div>
            @endif
            <div class="mt-4 col-12">
                <div>
                    <div class="card">
                    <div class="card-body">
                        <form method="post" action="/appointment">
                            @csrf
                        <input type="hidden" value="{{$date}}" name="dateTime">
                        <input type="hidden" value="{{$data['vet_data']->user_type->id}}" name="user_type_id">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="inputEmail4">{{__('Quem vai ser atendido?')}}</label>
                                <select class="form-control" required name="pet_id">
                                    @foreach($pets as $pet)
                                        <option value="{{$pet->id}}">
                                            {{$pet->name}}
                                        </option>
                                    @endforeach
                                </select>
                                </div>
                                <div class="form-group col-md-6">
                                <label for="inputPassword4">{{__('Método de Pagamento')}}</label>
                                <select class="form-control" required name="payment_method">
                                    <option value="1">Dinheiro</option>
                                    <option value="2">Débito</option>
                                    <option value="3">Crédito</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress">{{__('Valor Total')}}</label>
                                <h4>R$: {{$data['vet_data']->average_value}}</h4>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2">{{__('Observações')}}</label>
                                <textarea required name="adds" rows="5" class="form-control"></textarea>
                            </div>
                        <button type="submit" class="btn btn-secondary float-right">{{__('Confirmar')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection
