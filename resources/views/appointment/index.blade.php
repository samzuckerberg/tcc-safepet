@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body" style="background-color: #45B39D;">
                    <div class="d-flex">
                        <div class="col d-flex flex-column justify-content-center">
                            <h4 class="text-light line">{{__('ENCONTRE A ESPECIALIDADE QUE DESEJA')}}</h4>
                            <div class="">
                                <h5 class="pt-5">
                                    {{__('Temos clínicas de todos os tipos!')}}
                                </h5>
                            </div>
                        </div>
                        <img class="col-6" src="img/appointment-dog01.jpg" style="max-height: 250px">
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (\Session::get('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{\Session::get('success')}}</li>
                </ul>
            </div>
            @endif
            <div class="container mt-4">
                <div class="row">
                    @foreach($clinics as $clinic)
                    <div class="col-xl-4 col-lg-6 col-12 pb-4">
                    <a href="/appointment/{{$clinic->id}}"
                            class="btn btn-secondary btn-lg text-truncate w-100">{{$clinic->name}}</a>
                    </div>
                    @endforeach
                </div>
                <h2 class="mt-3">Consultas marcadas</h2>
                @foreach($pets as $pet)
                <h4 class="mt-3">{{$pet->name}}</h4>
                <div class="row">
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">{{__('Data e horário')}}</th>
                        <th scope="col">{{__('Método de pagamento')}}</th>
                        <th scope="col">{{__('Status')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (App\Scheduling::where('pet_id', $pet->id)->get() as $dataIndex => $data)
                        <tr>
                        <th scope="row">{{$dataIndex+1}}</th>
                        <td>{{$data->dateTime}}</td>
                        <td>@if($data->payment_method == 1)Dinheiro @endif
                        @if($data->payment_method == 2)Débito @endif
                        @if($data->payment_method == 3)Crédito @endif</td>
                        <td>@if($data->status)
                                <h5 disabled>Agendado</h5>
                            @else
                                <h5>Esperando</h5>
                            @endif
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    @endsection
