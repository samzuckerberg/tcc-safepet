@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h3>{{__('Cadastre o seu Pet: ')}}</h3>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" class="mt-4" action="/pets">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <h5 style="color: #45B39D;">{{__('Nome')}}</h5>
                            <input type="text" id="name" name="name" required value="{{ old('name') }}" class="form-control border-top-0 border-left-0 border-right-0">
                        </div>
                    </div>
                    <div class="form-row mt-4">
                        <div class="col">
                            <h5 style="color: #45B39D;">{{__('Data de nascimento')}}</h5>
                            <input type="date" id="birth" name="birth" required value="{{ old('birth') }}" class="form-control border-top-0 border-left-0 border-right-0"/>
                        </div>
                        <div class="col">
                            <h5 style="color: #45B39D;">{{__('Espécie')}}</h5>
                            <input type="text" id="species" name="species" required value="{{ old('species') }}" class="form-control border-top-0 border-left-0 border-right-0">
                        </div>
                    </div>
                    <div class="form-row mt-4 justify-content-center">
                        <button type="submit" class="btn btn-secondary w-25">{{__('Cadastrar')}}</button>
                    </div>

                </form>
            </div>
        </div>
@endsection

