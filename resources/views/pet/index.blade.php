@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body bg-secondary">
                    <div class="d-flex">
                        <div class="col d-flex flex-column justify-content-center">
                            <h4 class="text-light line">{{__('Registre os seus AUmigos aqui! ;)')}}</h4>
                            <div class="">
                                <h5 class="pt-5 text-light">
                                    {{__('Você vai cadastrar cada um dos seus AUmigos para facilitar a experiência no site!')}}
                                </h5>
                            </div>
                        </div>
                        <img class="col-6" src="img/pet-create.jpg" style="max-height: 250px">
                    </div>
                </div>
                <div class="container text-center">
                    <h5 class="mt-2" style="color: #45B39D;">{{__('Lista dos animais cadastrados')}}</h5>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (\Session::get('success'))
            <div class="alert alert-success mt-3">
                <span>{{\Session::get('success')}}</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="mt-4">
                @if($pets->isNotEmpty())
                <table class="table border">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Espécie</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pets as $petIndex => $pet)
                        <tr>
                            <th scope="row">{{$petIndex + 1}}</th>
                            <td><a href="/pets/{{$pet->id}}">{{$pet->name}}</a></td>
                            <td>{{$pet->species}}</td>
                            <td>
                                <form method="post" action="/pets/{{$pet->id}}">
                                    @csrf
                                    @method('delete')
                                    <div class="btn-group dropup">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false" class=" dropdown-toggle border-0 bg-transparent btn">
                                            <i class="fas fa-times-circle text-danger"></i>
                                        </button>
                                        <div class="dropdown-menu justify-content-center">
                                            <div class="dropdown-item disabled font-weight-bold text-dark">
                                                {{__('Deseja remover o seu AUmiguinho?')}}
                                            </div>
                                            <div class="dropdown-divider"></div>
                                            <div class="px-2 d-flex justify-content-center">
                                                <button class="p-0 btn btn-sm btn-primary example-popover px-3 "
                                                    type="submit" data-container="body" data-toggle="popover">
                                                    {{__('Sim')}}
                                                </button>
                                                <a href="#"
                                                    class="btn btn-sm btn-link text-dark ml-2 px-3">{{__('Não')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <h4>{{__('Você não possui AUmigos cadastrados!')}}</h4>
                @endif
                <a class="btn btn-primary mb-4 float-right" href="{{ url('/pets/create') }}">
                    {{__('Cadastrar!')}}
                </a>
            </div>
        </div>
    </div>
    @endsection
