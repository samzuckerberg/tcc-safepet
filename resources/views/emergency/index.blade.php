@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body bg-secondary">
                        <div class="d-flex">
                            <div class="col d-flex flex-column justify-content-center">
                                <h4 class="text-light line">{{__('VAMOS CUIDAR DO SEU AUMIGO')}}</h4>
                                <div class="">
                                    <h5 class="pt-5 text-light">{{__('Você vai escrever para a gente o que ele está sentindo e a gente já busca alguém para ajudar!')}}</h5>
                                </div>
                            </div>
                            <img class="col-6" src="img/emergency-dog.jpg" style="max-height: 250px">
                        </div>
                    </div>
                    <div class="container text-center">
                        <h5 class="mt-2" style="color: #45B39D;">{{__('Emergências antigas')}}</h5>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::get('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{\Session::get('success')}}</li>
                        </ul>
                    </div>
                @endif
                @if($pets->isNotEmpty())
                    <table class="table border">
                        <thead>
                        <tr>
                            <th scope="col">{{__('Data')}}</th>
                            <th scope="col">{{__('Número do chamado')}}</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($emergencies as $emergenciesIndex => $emergency)
                            <tr>
                                <th scope="row">{{$emergency->created_at}}</th>
                                <td>{{$emergency->id}}</td>
                                <td>
                                    <a href="{{ url('/emergency/' . $emergency->id) }}">
                                        <i class="fas fa-chevron-right text-primary"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <a class="btn btn-primary mb-4 float-right" href="{{ url('/emergency/create') }}">
                        {{__('Cadastrar!')}}
                    </a>
                @else
                    <div class="border">
                        <h4 class="mt-4">{{__('Você não possui AUmigos cadastrados!')}}</h4>
                        <a class="navbar-brand" href="{{ url('/pets/create') }}">
                            {{__('Clique aqui para ir até a página de cadastro!')}}
                        </a>
                    </div>
                @endif
            </div>
        </div>
@endsection

