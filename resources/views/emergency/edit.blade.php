@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card p-2">
                    <a href="{{ url('/emergency') }}">
                        <i class="fas fa-chevron-left text-primary"></i>
                    </a>
                    <div class="card-body ">
                        <div class="d-flex">
                            <div class="col d-flex flex-column justify-content-center list-group-horizontal">
                                <div class="row h-100 justify-content-center align-items-center">
                                    <form class="col-4">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">{{__('Data')}}</label>
                                            <label class="form-control text-secondary border-0"
                                                   id="formGroupExampleInput">{{$emergency->created_at}}</label>
                                        </div>
                                    </form>
                                    <form class="col-4">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">{{__('Número do chamado')}}</label>
                                            <label class="form-control text-secondary border-0"
                                                   id="formGroupExampleInput">{{$emergency->id}}</label>
                                        </div>
                                    </form>
                                    <form class="col-4">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">{{__('Status')}}</label>
                                            <label class="form-control text-secondary border-0"
                                                   id="formGroupExampleInput">{{$emergency->status == 'e' ? 'Esperando' : 'Atendido'}}</label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <form action="/emergency/{{$emergency->id}}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="col">
                                <label for="formGroupExampleInput">{{__('Informações adicionais')}}</label>
                                <textarea class="w-100 textArea" name="add_information" rows="3">{{$emergency->add_information}}</textarea>
                            </div>
                            <div class="form-row mt-3 justify-content-center">
                                <button type="submit" class="btn btn-secondary w-25">{{__('Enviar')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection

