@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body bg-secondary">
                        <div class="d-flex">
                            <div class="col d-flex flex-column justify-content-center">
                                <h4 class="text-light line">{{__('VAMOS CUIDAR DO SEU AUMIGO')}}</h4>
                                <div class="">
                                    <h5 class="pt-5 text-light">{{__('Você vai escrever para a gente o que ele está sentindo e a gente já busca alguém para ajudar!')}}</h5>
                                </div>
                            </div>
                            <img class="col-6" src="/img/emergency-dog.jpg" style="max-height: 250px">
                        </div>
                    </div>
                    <div class="container text-center">
                        <h5 class="mt-2" style="color: #45B39D;">{{__('Preencha as informações do seu pet')}}</h5>
                    </div>
                </div>
                @if($pets->isNotEmpty())
                    <form method="post" action="/emergency" class="mt-4">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                <select name="petId" class="custom-select" required>
                                    <option disabled>{{__('Selecione um AUmigo: ')}}</option>
                                    @foreach($pets as $pet)
                                        <option value="{{$pet->id}}">{{$pet->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                            </div>
                        </div>
                        <div class="form-row mt-4">
                            <div class="col">
                                <h5 style="color: #45B39D;">{{__('Como ele está se sentindo?')}}</h5>
                                <textarea required rows="7" name="about"
                                          class="form-control border-top-0 border-left-0 border-right-0"></textarea>
                            </div>
                        </div>
                        <div class="form-row mt-4 justify-content-center">
                            <button type="submit" class="btn btn-secondary w-25">{{__('Enviar')}}</button>
                        </div>
                    </form>
                @else
                    <div class="border">
                        <h4 class="mt-4">{{__('Você não possui AUmigos cadastrados!')}}</h4>
                        <a class="navbar-brand" href="{{ url('/pets/create') }}">
                            {{__('Clique aqui para ir até a página de cadastro!')}}
                        </a>
                    </div>
                @endif
            </div>
        </div>
@endsection

