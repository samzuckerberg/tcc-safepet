@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="d-flex">
                        <img class="col-6" src="img/home-dog-01.jpg" style="max-height: 250px">
                        <div class="col d-flex flex-column justify-content-between">
                            <h4 class="text-success">{{__('O SAFEPET ENTENDE AS NECESSIDADES DOS NOSSOS AUMIGOS')}}</h4>
                            <div class="">
                                <h5 class="align-self-end">{{__('Dá para fazer de tudo para ajudar nosso pets em um único lugar!')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body" style="background-color: #45B39D;">
                    <div class="d-flex">
                        <div class="col d-flex flex-column justify-content-center">
                            <h4 class="text-light line"><u>{{__('lojinha')}}</u></h4>
                            <div class="">
                                <h5 class="pt-5">{{__('Aqui dá para comprar tudo online. Fácil e rápido na sua casa.')}}</h5>
                            </div>
                            <button type="button" class="btn btn-secondary rounded mt-4">saiba mais</button>
                        </div>
                        <img class="col-6" src="img/food.jpg" style="max-height: 250px">
                    </div>
                </div>
                <div class="card-body bg-secondary">
                    <div class="d-flex">
                        <div class="col d-flex flex-column justify-content-center">
                            <h4 class="text-light line"><u>{{__('emergência')}}</u></h4>
                            <div class="">
                                <h5 class="pt-5 text-light">{{__('Você nos diz como o seu AUmigo está se sentindo e a gente já vai enviando para alguém especializado ;)')}}</h5>
                            </div>
                            <button type="button" class="btn rounded mt-4" style="background-color: #45B39D;">saiba mais</button>
                        </div>
                        <img class="col-6" src="img/home-dog-02.jpg" style="max-height: 250px">
                    </div>
                </div>
                <div class="card-body bg-light">
                    <div class="d-flex">
                        <div class="col d-flex flex-column justify-content-center">
                            <h4 class="line" style="color: #45B39D;"><u>{{__('consulta')}}</u></h4>
                            <div class="">
                                <h5 class="pt-5">{{__('A gente te mostra os veterinários mais próximos de você')}}</h5>
                            </div>
                            <button type="button" class="btn btn-secondary rounded mt-4">saiba mais</button>
                        </div>
                        <img class="col-6" src="img/home-dog-03.jpg" style="max-height: 250px">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
