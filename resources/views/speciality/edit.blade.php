@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3>{{__('Edite a sua especialidade!: ')}}</h3>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="post" class="mt-4" action="/specialties/{{$specialties->id}}">
                @csrf
                @method('PATCH')
                <div class="form-row">
                    <div class="col">

                        <h5 style="color: #45B39D;">{{__('Especialidade')}}</h5>
                        <select id="clinics_id" name="clinics_id" required value="{{ old('clinics_id') }}"
                            class="form-control border-top-0 border-left-0 border-right-0">
                            @foreach ($clinics as $item)
                            <option @if($specialties->clinics_id == $item->id) selected @endif
                                value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row mt-4">
                    <div class="col">
                        <h5 style="color: #45B39D;">{{__('Valor médio das consultas')}}</h5>
                        <input type="text" id="average_value" name="average_value" required
                            value="{{$specialties->average_value}}"
                            class="form-control border-top-0 border-left-0 border-right-0" />
                    </div>
                    <div class="col">
                        <h5 style="color: #45B39D;">{{__('Graduação')}}</h5>
                        <input type="text" id="university_gradutate" name="university_gradutate" required
                            value="{{$specialties->university_graduate}}"
                            class="form-control border-top-0 border-left-0 border-right-0">
                    </div>
                </div>
                <div class="form-row mt-4 justify-content-center">
                    <button type="submit" class="btn btn-secondary w-25">{{__('Editar')}}</button>
                </div>

            </form>
        </div>
    </div>
    @endsection
