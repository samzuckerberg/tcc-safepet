<?php
use Illuminate\Database\Seeder;
use App\Clinic;

class ClinicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Clinic::count()) {
            $array = [
                'Acupuntura', 'Análise Clínica', 'Clinica Geral',
                'Endocrinologia', 'Especialista em Felinos', 'Fisioterapia',
                'Floral Terapia', 'Homeopatia', 'Nutrologia', 'Odontologia',
                'Oftalmologia', 'Oncologia', 'Ozonioterapia', 'Quiropraxia', 'Silvestres e Exóticos', 'Ultrassonografia'
            ];

            foreach ($array as $item) {
                DB::table('clinics')->insert([
                    'name' => $item,
                ]);
            }
        }
    }
}
